# Postcode Scraper

Code to scrape Australian postcodes and suburb information from the Australia
Post website, written in R by Pete Abriani Jensen
[<pete@tychoanalytics.com>](mailto://pete@tychoanalytics.com)

### Getting Started

First, you will need to ensure that R is installed on your system. R is free 
software that can be obtained from the [CRAN](https://cran.r-project.org/)
website or using a package manager such as *brew* on Mac OSX, or *yum* or
*apt-get* on UNIX-like systems.

The following R packages also need to be installed:

* dplyr
* foreach
* stringr

These packages can be installed from R prompt using the command
`install.packages(c("dplyr", "stringr"))` or from the terminal by changing
directory into the install directory and executing the `setup` shell script.

### Executing the Code

Postcode scraper can be executed from the terminal, or interactively using the
R prompt. To execute from the terminal, simply `cd` into the install directory
and execute the `postcode-scraper` shell script. To execute interactively in R,
set the working directory to the install directory using `setwd` and run the
following commands:

* `source("code/scrape.R"")`
* `source("code/build.R"")`

The first script downloads the HTML pages from the Auspost website to the
data/html/[*date*] folder and the second script extracts the data from the HTML
files and saves them as a CSV file in the data folder.

By default, the most recent folder in data/html is used to construct the
postcode catalogue. You can build a catalogue from another folder by changing
the *date* parameter in code/build.R. The `build_catalogue` function in
code/functions.R also takes a date parameter and can be used to return a postcode
catalogue as a data frame.

Happy scraping!
